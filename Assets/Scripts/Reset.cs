﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour
{
    public void ResetPos()
    {
        foreach (var product in FindObjectsOfType<Product>())
        {
            product.IsUsed = false;
            product.transform.position = product.DefaultPos;
        }
        foreach (var wall in FindObjectsOfType<Wall>())
        {
            wall.IsSet = false;
        }
    }
}
