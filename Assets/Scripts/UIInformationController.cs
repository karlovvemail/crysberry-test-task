﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIInformationController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI topDistance, sideDistance;

    public void SetDistance(Information info)
    {
        topDistance.text = info.TopDistance.ToString();
        sideDistance.text = info.SideDistance.ToString();
    }
    
}
