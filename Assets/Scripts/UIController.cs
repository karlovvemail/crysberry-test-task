﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using TMPro;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class UIController : MonoBehaviour
{
    private Animator m_Animator;
    private TextMeshProUGUI title;

    [SerializeField] private SelectingController selectingController;

    private void Start()
    {
        m_Animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        Show();
    }

    public void Confirm()
    {
        Hide();
        selectingController.ConfirmPlacing();
    }

    public void Cancel()
    {
        Hide();
        selectingController.CancelPlacing();
    }

    public void Show()
    {
        StartCoroutine(ScaleAsync(Vector3.one, 1f));
    }
    
    public void SetText(String message)
    {
        title.text = message;
    }

    public void Hide()
    {
        StartCoroutine(ScaleAsync(Vector3.zero,1f,()=>gameObject.SetActive(false)));
    }

    private IEnumerator ScaleAsync(Vector3 scaleTo, float seconds, Action callback = null)
    {
        float elapsedTime = 0;
        Vector3 initScale = transform.localScale;
        while (elapsedTime < seconds)
        {
            transform.localScale = Vector3.Lerp(initScale, scaleTo, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        transform.localScale = scaleTo;
        callback?.Invoke();
    }
}