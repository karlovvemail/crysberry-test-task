﻿using UnityEngine;

public class Wall : MonoBehaviour
{
    public bool IsSet { get; set; }

    public Vector3 GetCenter()
    {
        var centre = GetComponent<Renderer>().bounds.min;
        centre.x = GetComponent<Renderer>().bounds.center.x;
        centre.y = GetComponent<Renderer>().bounds.min.y;
        centre.z = GetComponent<Renderer>().bounds.center.z;
        Debug.Log(centre);
        return centre;
    }


}