﻿using UnityEngine;

public class Product : MonoBehaviour
{
    private Renderer render;
    
    //information about placing product
    public bool IsUsed { get; set; }
    //distance to corner 
    public Vector3 Distance { get; set; }
    public Vector3 DefaultPos { get; set; }


    void Start()
    {
        render = GetComponent<Renderer>();
        DefaultPos = gameObject.transform.localPosition;
    }

    public void Highlight()
    {
        foreach (var componentsInChild in GetComponentsInChildren<Renderer>())
        {
            componentsInChild.material.SetFloat("_Outline", 1.03f);
        }
    }

    public void UnHighlight()
    {
        foreach (var componentsInChild in GetComponentsInChildren<Renderer>())
        {
            componentsInChild.material.SetFloat("_Outline", 1);
        }

    }
}