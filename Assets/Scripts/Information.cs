﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Information 
{
    private float sideDistance, topDistance;

    public Information(float sideDistance, float topDistance)
    {
        this.sideDistance = sideDistance;
        this.topDistance = topDistance;
    }

    public float SideDistance
    {
        get => sideDistance;
        set => sideDistance = value;
    }

    public float TopDistance
    {
        get => topDistance;
        set => topDistance = value;
    }
}
