﻿using UnityEngine;

public class SelectingController : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private UIController confiramatioinPanel ;
    [SerializeField] private UIInformationController uiInformation ;

    
    private Product selectedProduct;
    private Wall selectedWall;
    private Vector3 distance;
    void Update()
    {
        var ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out var hit))
        {
            Debug.DrawRay(camera.transform.position, hit.point, Color.green);
            var selection = hit.transform;
            if (selection.GetComponent<Wall>())
                PlaceProduct(selection.GetComponent<Wall>(), hit.normal);

            if (!selection.GetComponent<Product>() || !Input.GetMouseButtonUp(0)) return;
                SelectProduct(selection.GetComponent<Product>());
        }
    }
    

    private void PlaceProduct(Wall wall,Vector3 normal)
    {
        if(!Input.GetMouseButtonUp(0) || wall.IsSet || !selectedProduct || selectedProduct.IsUsed) return;
        selectedProduct.transform.position = wall.GetCenter()+ CalculateNormalOffset(normal,selectedProduct.GetComponent<Collider>());
        selectedProduct.UnHighlight();
        CalculateDistance(normal, wall);
        confiramatioinPanel.gameObject.SetActive(true);
        camera.GetComponent<CameraController>().IsLoock = true;
        selectedWall = wall;
        wall.IsSet = true;
    }

    private void SelectProduct(Product product)
    {
        if (selectedProduct)
            selectedProduct.UnHighlight();
        selectedProduct = product;
        selectedProduct.Highlight();
    }

    private Vector3 CalculateNormalOffset(Vector3 normal, Collider obj)
    {
        switch (obj)
        {
            case BoxCollider _:
                var product = obj.GetComponent<BoxCollider>().bounds.size/2;
                //  prpduct.y = obj.GetComponent<BoxCollider>().bounds.center.y;
                var scaled = Vector3.Scale(product, normal);
                scaled.y = scaled.y + product.y;
                return scaled;
            case SphereCollider _:
                return normal * obj.GetComponent<SphereCollider>().radius;
            default:
                return Vector3.zero;
        }
    }
    
    private void CalculateDistance(Vector3 normal, Wall wall)
    {
        distance = wall.GetCenter()  - CalculateNormalOffset(normal, selectedProduct.GetComponent<Collider>());
        Debug.Log(distance );
    }

    //stop placing(when click Cancel button)
    public void CancelPlacing()
    {
        selectedProduct.transform.localPosition = selectedProduct.DefaultPos;
        selectedProduct = null;
        camera.GetComponent<CameraController>().IsLoock = false;
        selectedWall.IsSet = !selectedWall.IsSet;
        selectedWall = null;
        
    }
    
    public void ConfirmPlacing()
    {
        //set distance information to confirmed product
        uiInformation.GetComponent<UIInformationController>()
            .SetDistance(new Information(Mathf.Abs(distance.x), Mathf.Abs(distance.y)));
        selectedProduct.Distance = distance;
        
        //unlock camera mouth look
        camera.GetComponent<CameraController>().IsLoock = false;
        
        //lock product for further further use
        selectedProduct.IsUsed = !selectedProduct.IsUsed;
        
        selectedProduct = null;
        selectedWall.IsSet = true;
        selectedWall = null;
    }
    
}